# Bloggy

The poor man's blog, with raw JEE.

## **/!\\** Do not use this project. **/!\\**

This project is intended for learning purposes and does not have the security
requirements needed to run in production, sometimes on purpose.


## Development Requirements

* Java 8+
* Maven
* Project Lombok

### Lombok

Bloggy is relying on [project Lombok](https://projectlombok.org/) to generate
parts of its sources (the most annoying parts). Therefore, you might need
the appropriate plugin for your IDE:
* For VSCode/VSCodium: https://marketplace.visualstudio.com/items?itemName=GabrielBB.vscode-lombok
* For Netbeans: https://projectlombok.org/setup/netbeans
* For IntelliJ IDEA: https://projectlombok.org/setup/intellij


## Setup

You may use one of two options, either use the Vagrant configuration presented
in the project or use your local Tomcat and MySQL installation.


### Using Vagrant

Vagrant is a command-line tool to define, manipulate and run per-project
virtual machines on a development environement.

Vagrnat relies on the presence of a compatible Hypervisor. Here, you'll
need VirtualBox.

* [VirtualBox](https://www.virtualbox.org/)
* [Vagrant](https://www.vagrantup.com/) (installation may require rebooting
  the system)

One equipped, you don't even need Java, Tomcat or MySQL on your machine
to run or deploy the project.

For the project's root, run:
```bash
vagrant up
```
It may take a while, especially the first time. Go make yourself a nice
cup of tea and come back: you should have a fully working virtual machine,
which run Tomcat 9, MariaDB, and contains a OpenJDK 11 JDK and Maven.

As the Vagrantfile stipulates, the VM is exposed on your machine with the
local IP 192.168.33.10.

* Tomcat is visible on http://192.168.33.10:8080/
* Tomcat Application manager is on http://192.168.33.10:8080/manager/html
* The application should be available at http://192.168.33.10:8080/bloggy
* The database can be access through 192.168.33.10:3306


You may connect to the machine with:
```bash
vagrant ssh
```

Inside the virtual machine the project directory is mapped to `/vagrant`.
So you may go there and, for instance, build and deploy the application:

```bash
cd /vagrant
mvn package cargo:redeploy
```

In case of error, you may re-run the setup with
```bash
vagrant provision
```

And stop the virtual machine with
```bash
vagrant halt
```

And completely start fresh with
```bash
vagrant destroy --force && vagrant up
```

#### Administering the database

The setup installs a MariaDB instance. The defautl configuration is tweaked
to allow remote connections (unlike most cases on an actual server). A
dedicated user `bloggy` with the password `secret` is created by
`provision\create-db-and-user.sql`.

`provision\create-admin.sql` create a user called `admin` with password
`ultrasecret` and has all privileges.

You may use your favorite database maagement software. Ensure the VM is
running and connect to `192.168.33.10:3306` with the `admin` previously
mentionned.

The database's `root` is unavailable. You may access it from the inside
of the VM. Connect with `vagrant ssh` and start the mysql/mariadb client
with superuser privileges `sudo mysql`. No password will be required.


### Using a local Database and Tomcat installation

Let's srtup a local Mysql and Tomcat.

At the moment, the configuration in `src\main\webapp\META-INF\context.xml`
expects the database to be named `bloggy`.

1. First off, create a database on your local MySQL/MariaDB instance and
   run `db/schema.sql` on it.

2. You probably want a few test data along the schema, run `db/test-data.sql`

   ```bash
   $ cd db
   $ mysql -u root -p -e "
     CREATE DATABASE bloggy;
     use bloggy;
     source schema.sql
     source test-data.sql
   "
   ```

3. Ensure your `~/.m2/settings.xml` file contains a profile defining Tomcat
   and MySQL access. For instance, something like the following:
   ```xml
   <activeProfiles>
       <activeProfile>localT9</activeProfile>
   </activeProfiles>
   <profiles>
       <profile>
           <id>localT9</id>
           <properties>
               <tomcat.manager.hostname>localhost</tomcat.manager.hostname>
               <tomcat.servlet.port>8080</tomcat.servlet.port>
               <tomcat.username>cargo</tomcat.username>
               <tomcat.password>cargo</tomcat.password>

               <mysql.user>root</mysql.user>
               <mysql.password>root</mysql.password>
               <mysql.url>jdbc:mysql://localhost:3306/</mysql.url>
           </properties>
       </profile>
   </profiles>
   ```

4. Ensure the tomcat access user and password match that of
   `${CATALINA_HOME}/conf/tomcat-users.xml`. According to the previous example, you should find somthing like:
   ```xml
   <?xml version='1.0' encoding='cp1252'?>
   <tomcat-users xmlns="http://tomcat.apache.org/xml"
                 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                 xsi:schemaLocation="http://tomcat.apache.org/xml tomcat-users.xsd"
                 version="1.0">

   <role rolename="manager-gui"/>
   <role rolename="manager-script"/>
   <user username="root" password="root" roles="admin-gui,manager-gui" />
   <user username="cargo" password="cargo" roles="manager-script" />

   </tomcat-users>
   ```
   (if you were to change this file, remember to restart tomcat)

5. Try it. Run:
   ```bash
   mvn clean package cargo:deploy
   ```
   ... and navigate to `http://localhost:8080/bloggy`

   Note that the test data define two users:
   ```
   name: admin
   password: ultrasecret
   ```
   with the password
   ```
   name: normal
   password: secret
   ```
