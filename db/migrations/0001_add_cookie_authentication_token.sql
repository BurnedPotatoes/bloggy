ALTER TABLE `user`
ADD COLUMN `cookieAuthenticationToken` CHAR(50) NULL AFTER `password`,
ADD COLUMN `cookieAuthenticationTokenExpiration` DATETIME NULL AFTER `cookieAuthenticationToken`,
ADD UNIQUE INDEX `cookieAuthenticationToken_UNIQUE` (`cookieAuthenticationToken` ASC) VISIBLE;

