#!/bin/bash
set -e
set -u

TOMCAT_URL="https://mirrors.ircam.fr/pub/apache/tomcat/tomcat-9/v9.0.37/bin/apache-tomcat-9.0.37.tar.gz"
TOMCAT_DIR="/opt/tomcat9"
VAGRANT_DIR="/vagrant"

log() {
    echo ">>>" "$@"
}

setup-jdk11() {
    apt-get install -qq -y \
        openjdk-11-jre-headless \
        openjdk-11-jdk
}

setup-maven() {
    apt-get install -qq -y maven
    cp /vagrant/provision/settings.xml /usr/share/maven/conf/
}

setup-mariadb() {
    apt-get install -qq -y mariadb-{server,client}

    sed -i "s/.*bind-address.*/bind-address = 0.0.0.0/" \
        "/etc/mysql/mariadb.conf.d/50-server.cnf"
    sudo ufw allow 3306/tcp
    systemctl restart mariadb

    mysql < "/vagrant/provision/create-admin.sql"
    mysql < "/vagrant/provision/create-db-and-user.sql"
    mysql bloggy < "/vagrant/db/schema.sql"
    mysql bloggy < "/vagrant/db/test-data.sql"
}

setup-tomcat9() {
    # We won't install tomcat from the repository as it doesn't contain
    # the manager webapp, and needs configuration anyway
    wget -q -O "/tmp/tomcat9.tgz" "${TOMCAT_URL}"
    mkdir -p "${TOMCAT_DIR}"
    tar xzf "/tmp/tomcat9.tgz" -C "${TOMCAT_DIR}" --strip-components=1

    # User, Group and permissions
    getent group tomcat \
        || groupadd tomcat
    getent passwd tomcat \
        || useradd -s /bin/false -g tomcat -d /opt/tomcat tomcat

    chgrp -R tomcat "${TOMCAT_DIR}"
    (cd "${TOMCAT_DIR}" &&
        sudo chmod -R g+r conf &&
        chmod g+x conf &&
        chown -R tomcat webapps/ work/ temp/ logs/
    )

    # Configuration
    cp "${VAGRANT_DIR}/provision/tomcat-users.xml" \
        "${TOMCAT_DIR}/conf/tomcat-users.xml"
    cp "${VAGRANT_DIR}/provision/manager-context.xml" \
        "${TOMCAT_DIR}/webapps/manager/META-INF/context.xml"

    # systemd Service
    ufw allow 8080/tcp
    cp /vagrant/provision/tomcat9.service \
        /etc/systemd/system/tomcat9.service
    systemctl daemon-reload
    systemctl enable tomcat9
    systemctl start tomcat9
}

main() {
    apt-get update -qq

    log "Setup JDK"
    setup-jdk11
    log "Done"

    log "Setup Maven"
    setup-maven
    log "Done"

    log "Setup MariaDB"
    setup-mariadb
    log "Done"

    log "Setup Tomcat9"
    setup-tomcat9
    log "Done"

    log "Build and Deploy app"
    (cd /vagrant &&
        # cargo:redploy has the advantage working when the provision is
        # relaunched, and won't fail if the application is not already present.
        su -c "mvn package cargo:redeploy"
    )
}

main "$@"
