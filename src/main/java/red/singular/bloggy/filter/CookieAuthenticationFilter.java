package red.singular.bloggy.filter;

import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDateTime;

import javax.naming.NamingException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
// import javax.servlet.http.HttpServletResponse;

import lombok.extern.slf4j.Slf4j;

import red.singular.bloggy.models.User;
import red.singular.bloggy.dao.UserDAO;
import red.singular.bloggy.util.DbHelper;
import red.singular.bloggy.util.FlashHelper;

@Slf4j @WebFilter(filterName="cookie-authentication-filter")
public class CookieAuthenticationFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {}

    @Override
    public void destroy() {}

    @Override
    public void doFilter(
        ServletRequest request,
        ServletResponse response,
        FilterChain chain
    ) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        // HttpServletResponse httpResponse = (HttpServletResponse) response;

        Cookie authToken = this.getAuthTokenCookie(httpRequest);

        if (authToken != null) {
            try {
                UserDAO userDAO = new UserDAO(DbHelper.getDataSource());
                User user = userDAO.findUserByCookieAuthenticationToken(
                    authToken.getValue());

                if (user != null) {
                    if (user.getCookieAuthenticationTokenExpiration()
                            .isAfter(LocalDateTime.now())) {
                        httpRequest.getSession().setAttribute("user", user);
                        log.info("Authenticated User <{}> with token <{}>",
                            user.getUsername(),
                            user.getCookieAuthenticationToken());
                    }
                    else { // Token is outdated, let's remove it from the database
                        user.setCookieAuthenticationToken(null);
                        user.setCookieAuthenticationTokenExpiration(null);
                        userDAO.update(user);
                        FlashHelper.addInfo(
                            "Your authentication expired, please log in again",
                            httpRequest
                        );
                        log.info("User <{}> token's went outdated",
                            user.getUsername());
                    }
                }
                else {
                    log.info("No User were found for the token <{}>",
                        authToken.getValue());
                }
            }
            catch (SQLException sqle) {
                throw new ServletException(sqle);
            }
            catch (NamingException ne) {
                throw new ServletException(ne);
            }
        }
        chain.doFilter(request, response);
    }

    private Cookie getAuthTokenCookie(HttpServletRequest request) {
        Cookie authToken = null;

        if (request.getCookies() != null) {
            for (Cookie cookie: request.getCookies()) {
                if (cookie.getName().equals("auth-token")) {
                    authToken = cookie;
                    break;
                }
            }
        }

        return authToken;
    }
}
