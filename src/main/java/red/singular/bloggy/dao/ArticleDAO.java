package red.singular.bloggy.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import lombok.extern.slf4j.Slf4j;
import red.singular.bloggy.models.Article;

@Slf4j
public class ArticleDAO {
    private static final DateTimeFormatter formatter =
        DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    private static final String FIELD_LIST =
        "id,"
        + "body,"
        + "created,"
        + "title ";

    public static final String SELECT_BY_ID =
        "SELECT "
        + FIELD_LIST
        + "FROM article WHERE id = ?";

    public static final String SELECT_ALL =
        "SELECT "
        + FIELD_LIST
        + "FROM article ORDER BY created DESC LIMIT ?, ?";

    public static final String INSERT =
        "INSERT INTO article (title, body, created) VALUES (?, ?, ?)";

    public static final String SELECT_COUNT = "SELECT COUNT(*) FROM article";

    private DataSource datasource;

    public ArticleDAO(DataSource datasource) {
        this.datasource = datasource;
    }

    protected Article resultSetToArticle(ResultSet results)
        throws SQLException {

        Article article = new Article();

        article.setId(results.getLong(1));
        article.setBody(results.getString(2));
        article.setCreated(results.getTimestamp(3).toLocalDateTime());
        article.setTitle(results.getString(4));

        return article;
    }

    public Article findById(long id) throws SQLException {
        Article article = null;

        log.debug("Find article {} with query: {}", id, SELECT_BY_ID);

        try (
            Connection connection = this.datasource.getConnection();
            PreparedStatement statement =
                connection.prepareStatement(SELECT_BY_ID);
        ) {
            statement.setLong(1, id);
            try (ResultSet results = statement.executeQuery()) {
                if (results.next()) {
                    article = this.resultSetToArticle(results);
                }
            }
        }

        return article;
    }

    public List<Article> findAll(int page, int pageSize) throws SQLException {
        List<Article> articleList = new ArrayList<>();
        int first = (page - 1) * pageSize + 1;

        log.debug("Find articles from {} to {} with query: {}",
            first,
            first + pageSize,
            SELECT_ALL);

        try (
            Connection connection = this.datasource.getConnection();
            PreparedStatement statement =
                connection.prepareStatement(SELECT_ALL);
        ) {
            statement.setInt(1, first);
            statement.setInt(2, pageSize);

            try (ResultSet results = statement.executeQuery()) {
                while (results.next()) {
                    articleList.add(resultSetToArticle(results));
                }
            }
        }

        return articleList;
    }


    public void saveArticle(Article article) throws SQLException {
        log.debug("Create an article ({}) with query: {}", article, INSERT);

        try (
            Connection connection = this.datasource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                INSERT,
                Statement.RETURN_GENERATED_KEYS);
        ) {
            statement.setString(1, article.getTitle());
            statement.setString(2, article.getBody());
            statement.setString(3, formatter.format(article.getCreated()));

            int affectedRows = statement.executeUpdate();

            if (affectedRows == 0) {
                throw new SQLException(
                    "Article creation failed, no rows affected");
            }

            try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    article.setId(generatedKeys.getInt(1));
                }
                else {
                    throw new SQLException(
                        "Article creation failed, no id obtained");
                }
            }
        }
    }

    public long getCount() throws SQLException {
        try (
            Connection connection = this.datasource.getConnection();
            PreparedStatement statement =
                connection.prepareStatement(SELECT_COUNT);
            ResultSet results = statement.executeQuery()
        ) {
            results.next();
            return results.getLong(1);
        }
    }
}
