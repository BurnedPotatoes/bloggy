package red.singular.bloggy.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import javax.sql.DataSource;

import red.singular.bloggy.models.User;


public class UserDAO {

    private final String FIELD_LIST =
        "id,"
        + "username,"
        + "password,"
        + "cookieAuthenticationToken,"
        + "cookieAuthenticationTokenExpiration ";

    public final String SELECT_USER_BY_USERNAME_AND_PASSWORD =
        "SELECT "
        + FIELD_LIST
        + "FROM bloggy.user WHERE username = ? AND password = ?";

    public final String SELECT_USER_BY_COOKIE_AUTHENTICATION_TOKEN =
        "SELECT "
        + FIELD_LIST
        + "FROM bloggy.user WHERE cookieAuthenticationToken = ?";

    public final String UPDATE_USER =
        "UPDATE bloggy.user SET "
        + "username = ?,"
        + "password = ?,"
        + "cookieAuthenticationToken = ?,"
        + "cookieAuthenticationTokenExpiration = ? "
        + "WHERE id = ?";


    private DataSource datasource;

    public UserDAO(DataSource datasource) {
        this.datasource = datasource;
    }

    private User resultSetToUser(ResultSet results) throws SQLException {
        User user = new User();

        user.setId(results.getLong(1));
        user.setUsername(results.getString(2));
        user.setPassword(results.getString(3));
        user.setCookieAuthenticationToken(results.getString(4));

        if (results.getTimestamp(5) != null) {
            user.setCookieAuthenticationTokenExpiration(
                results.getTimestamp(5).toLocalDateTime()
            );
        }

        return user;
    }

    public User findUser(String username, String password) throws SQLException {
        User foundUser;

        try (
            Connection connection = this.datasource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                SELECT_USER_BY_USERNAME_AND_PASSWORD)
        ) {
            statement.setString(1, username);
            statement.setString(2, password);

            try ( ResultSet results = statement.executeQuery() ) {
                if (results.next()) {
                    foundUser = this.resultSetToUser(results);
                }
                else {
                    throw new SQLException(
                        String.format(
                            "User not found: %s:%s",
                            username,
                            password));
                }
            }
        }

        return foundUser;
    }

    /**
     * Find the User coresponding to the given Cookie Authentication Token.
     * Those token are unique in the table. Return the user or null.
     *
     * @param cookieAuthenticationToken
     * @return
     * @throws SQLException
     */
    public User findUserByCookieAuthenticationToken(
            String cookieAuthenticationToken
        ) throws SQLException {

        User foundUser = null;

        try (
            Connection connection = this.datasource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                SELECT_USER_BY_COOKIE_AUTHENTICATION_TOKEN)
        ) {
            statement.setString(1, cookieAuthenticationToken);

            try ( ResultSet results = statement.executeQuery() ) {
                if (results.next()) {
                    foundUser = this.resultSetToUser(results);
                }
            }
        }

        return foundUser;
    }

    public void update(User user) throws SQLException {
        try (
            Connection connection = this.datasource.getConnection();
            PreparedStatement statement
                = connection.prepareStatement(UPDATE_USER)
        ) {
            statement.setString(1, user.getUsername());
            statement.setString(2, user.getPassword());
            statement.setString(3, user.getCookieAuthenticationToken());
            if (user.getCookieAuthenticationTokenExpiration() != null) {
                Timestamp timestamp = Timestamp.valueOf(
                    user.getCookieAuthenticationTokenExpiration());
                statement.setTimestamp(4, timestamp);
            }
            else {
                statement.setTimestamp(4, null);
            }
            statement.setLong(5, user.getId());

            statement.executeUpdate();
        }
    }
}
