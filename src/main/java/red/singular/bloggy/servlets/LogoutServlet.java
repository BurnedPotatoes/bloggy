package red.singular.bloggy.servlets;

import java.io.IOException;
import java.sql.SQLException;

import javax.annotation.Resource;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import red.singular.bloggy.models.User;
import red.singular.bloggy.dao.UserDAO;
import red.singular.bloggy.util.CookieHelper;
import red.singular.bloggy.util.FlashHelper;

@WebServlet("/logout")
public class LogoutServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private UserDAO userDAO;

    @Resource(name="jdbc/bloggy")
    DataSource datasource;

    @Override
    public void init() throws ServletException {
        try {
            if (datasource == null) {
                Context initCtx = new InitialContext();
                Context envCtx = (Context) initCtx.lookup("java:comp/env");
                this.datasource = (DataSource) envCtx.lookup("jdbc/bloggy");
            }
        }
        catch (NamingException ne) {
            throw new ServletException(ne);
        }

        this.userDAO = new UserDAO(this.datasource);
    }

    @Override
    protected void doGet(
        HttpServletRequest req,
        HttpServletResponse resp
    ) throws ServletException, IOException {
        if (req.getSession().getAttribute("user") != null) {
            User user = (User) req.getSession().getAttribute("user");
            req.getSession().removeAttribute("user");
            resp.addCookie(CookieHelper.createAuthTokenCookieDeletion());
            user.setCookieAuthenticationToken(null);
            user.setCookieAuthenticationTokenExpiration(null);

            try {
                this.userDAO.update(user);
            }
            catch (SQLException sqle) {
                throw new ServletException(sqle);
            }
        }
        FlashHelper.addInfo("You successfully logged out", req);
        resp.sendRedirect(req.getContextPath() + "/login");
    }
}
