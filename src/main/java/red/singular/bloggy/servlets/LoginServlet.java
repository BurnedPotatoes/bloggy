package red.singular.bloggy.servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDateTime;

import javax.annotation.Resource;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import red.singular.bloggy.models.User;
import red.singular.bloggy.dao.UserDAO;
import red.singular.bloggy.util.CookieHelper;
import red.singular.bloggy.util.FlashHelper;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
    private static final long serialVersionUID = 5550148080346056149L;

    private UserDAO userDAO;

    @Resource(name="jdbc/bloggy")
    DataSource datasource;

    @Override
    public void init() throws ServletException {
        try {
            if (datasource == null) {
                Context initCtx = new InitialContext();
                Context envCtx = (Context) initCtx.lookup("java:comp/env");
                this.datasource = (DataSource) envCtx.lookup("jdbc/bloggy");
            }
        }
        catch (NamingException ne) {
            throw new ServletException(ne);
        }

        this.userDAO = new UserDAO(this.datasource);
    }

    @Override
    protected void doGet(
        HttpServletRequest req,
        HttpServletResponse resp
    ) throws ServletException, IOException {
        req.getRequestDispatcher("WEB-INF/jsp/pages/login.jsp")
            .forward(req, resp);
    }

    @Override
    protected void doPost(
        HttpServletRequest req,
        HttpServletResponse resp
    ) throws ServletException, IOException {

        String username = req.getParameter("username");
        String password = req.getParameter("password");

        try {
            User user = this.userDAO.findUser(username, password);

            // Authentication is successful
            FlashHelper.addMessage("Authentication Successful", req);

            HttpSession session = req.getSession();
            session.setAttribute("user", user);

            if (req.getParameter("stay-logged-in") != null
                && req.getParameter("stay-logged-in").equals("true")) {

                String token = CookieHelper.generateToken();
                resp.addCookie(CookieHelper.createAuthTokenCookie(token));

                user.setCookieAuthenticationToken(token);
                user.setCookieAuthenticationTokenExpiration(
                    LocalDateTime.now().plusSeconds(CookieHelper.AUTH_TOKEN_LIFESPAN)
                );

                userDAO.update(user);
            }

            if (req.getParameter("dest") != null) {
                resp.sendRedirect(req.getContextPath() + req.getParameter("dest"));
            }
            else {
                req.getRequestDispatcher("WEB-INF/jsp/pages/login.jsp")
                    .forward(req, resp);
            }
        }
        catch (SQLException sqle) {
            // FlashHelper.addMessage("Login Failed", req);
            // req.getRequestDispatcher("WEB-INF/jsp/pages/login.jsp")
            //     .forward(req, resp);
            throw new ServletException(sqle);
        }
    }
}
