package red.singular.bloggy.servlets.article.admin;

import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDateTime;

import javax.annotation.Resource;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import red.singular.bloggy.models.Article;
import red.singular.bloggy.dao.ArticleDAO;
import red.singular.bloggy.util.DbHelper;
import red.singular.bloggy.util.FlashHelper;

@WebServlet("/article/create")
public class AdminCreateArticleServlet extends HttpServlet {
    private static final long serialVersionUID = -274469182366198628L;

    @Resource(name="jdbc/bloggy")
    DataSource datasource;

    ArticleDAO articleDAO;

    @Override
    public void init() throws ServletException {
        try {
            if (datasource == null) {
                this.datasource = DbHelper.getDataSource();
            }
        }
        catch (NamingException ne) {
            throw new ServletException(ne);
        }
        this.articleDAO = new ArticleDAO(this.datasource);
    }

    @Override
    protected void doGet(
        HttpServletRequest req,
        HttpServletResponse resp
    ) throws ServletException, IOException {

        req.getRequestDispatcher("/WEB-INF/jsp/pages/createArticle.jsp")
            .forward(req, resp);
    }

    @Override
    protected void doPost(
        HttpServletRequest req,
        HttpServletResponse resp
    ) throws ServletException, IOException {

        Article newArticle = new Article();

        newArticle.setTitle(req.getParameter("title"));
        newArticle.setBody(req.getParameter("body"));
        newArticle.setCreated(LocalDateTime.now());

        try {
            this.articleDAO.saveArticle(newArticle);

            FlashHelper.addSuccess(
                "Article [" + newArticle.getTitle() + "] Successful", req);
            resp.sendRedirect(req.getContextPath());
        }
        catch (SQLException sqle) {
            throw new ServletException(sqle);
        }

    }

}
