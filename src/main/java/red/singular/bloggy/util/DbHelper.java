package red.singular.bloggy.util;

import javax.sql.DataSource;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public final class DbHelper {
    private DbHelper() {
        throw new UnsupportedOperationException(
            "This class is not meant to be instanciated");
    }

    public static DataSource getDataSource() throws NamingException {
        Context initCtx = new InitialContext();
        Context envCtx = (Context) initCtx.lookup("java:comp/env");
        return (DataSource) envCtx.lookup("jdbc/bloggy");
    }
}
