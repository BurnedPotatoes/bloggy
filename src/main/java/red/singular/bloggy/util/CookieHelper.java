package red.singular.bloggy.util;

import javax.servlet.http.Cookie;

public final class CookieHelper {
    public static final int AUTH_TOKEN_LIFESPAN = 7 * 24 * 60 * 60; // 7 days, in seconds

    private CookieHelper() {
        throw new UnsupportedOperationException(
            "This class is not meant to be instanciated");
    }

    public static String generateToken() {
        return TokenHelper.generateToken(50);
    }

    public static Cookie createAuthTokenCookie(String token) {
        Cookie authTokenCookie = new Cookie("auth-token", token);
        authTokenCookie.setHttpOnly(true);
        authTokenCookie.setMaxAge(AUTH_TOKEN_LIFESPAN);

        return authTokenCookie;
    }

    public static Cookie createAuthTokenCookieDeletion() {
        Cookie authTokenCookie = new Cookie("auth-token", null);
        authTokenCookie.setMaxAge(0);

        return authTokenCookie;
    }
}
