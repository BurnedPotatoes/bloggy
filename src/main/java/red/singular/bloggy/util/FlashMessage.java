package red.singular.bloggy.util;

import lombok.Data;
import lombok.NonNull;

@Data
public class FlashMessage {
    public enum Level {
        SUCCESS,
        INFO,
        WARNING,
        ERROR
    }

    private @NonNull Level level;
    private @NonNull String message;
}
