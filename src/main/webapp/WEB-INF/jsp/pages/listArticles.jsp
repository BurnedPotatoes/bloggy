<%@ page
  info="List all articles"
  contentType="text/html; charset=UTF-8"
  pageEncoding="UTF-8"
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:import url = "../_header.jsp"/>

<table class="table">
  <thead>
    <tr>
      <th>Title</th>
      <th>Creation date</th>
      <th>Actions</th>
    </tr>
  </thead>
  <tbody>
    <c:forEach var="article" items="${ articles }">
      <tr>
        <td>${article.title}</td>
        <td>${article.created}</td>
        <td>
          <a href="<c:url value="/article/delete?id=${article.id}" />" >
            Delete
          </a>
        </td>
      </tr>
    </c:forEach>
  </tbody>
</table>



<c:import url = "../_footer.jsp"/>
