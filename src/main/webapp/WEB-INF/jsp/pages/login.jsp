<%@ page
  info="LogIn form"
  contentType="text/html; charset=UTF-8"
  pageEncoding="UTF-8"
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:import url = "../_header.jsp"/>

<c:if test="${user == null }">
  <section class="section">
      <h2 class="title is-4">Log In</h2>
      <form class="box" action="#" method="post">

        <div class="field">
          <label class="label" for="username-input">Username</label>
          <div class="control">
            <input name="username" id="username-input" type="text" class="input:text" required>
          </div>
        </div>
        <div class="field">
          <label class="label" for="password-input">Password</label>
          <div class="control">
            <input name="password" id="password-input" type="password" class="input:text" required>
          </div>
        </div>

        <div class="field">
          <label class="checkbox">
            <input type="checkbox" value="true" name="stay-logged-in">
            Remember me
          </label>
        </div>

        <div class="field is-grouped">
          <div class="control">
            <button class="button is-link">Log In</button>
          </div>
        </div>
      </form>
  </section>
</c:if>

<c:if test="${user != null }">
  <section class="section">
    <p>
      You are already logged in
    </p>

    <p>
      <a class="button is-warning" href="<c:url value="/logout" />">Log Out</a>
    </p>
  </section>
</c:if>

<c:import url = "../_footer.jsp"/>
