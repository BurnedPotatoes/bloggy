<%@ page
  info="Article creation form"
  contentType="text/html; charset=UTF-8"
  pageEncoding="UTF-8"
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="red.singular.bloggy.util.TokenHelper" %>

<c:import url = "../_header.jsp"/>

<section class="section">
  <h2 class="title is-4">Create an Article</h2>

  <form class="box" action="#" method="post">
    <c:set var="csrfToken" value="${ TokenHelper.generateCsrfToken() }" />
    <c:set var="_csrfToken" value="${csrfToken}" scope="session" />
    <input type="hidden" value="${csrfToken}" name="${ TokenHelper.CSRF_TOKEN_VALUE_NAME }" />

    <div class="field">
      <label class="label" for="title-input">Title</label>
      <div class="control">
        <input name="title" id="title-input" type="text" class="input:text" required>
      </div>
    </div>

    <div class="field">
      <label class="label">Body</label>
      <div class="control">
        <textarea name="body" class="textarea" placeholder="Textarea" required></textarea>
      </div>
    </div>

    <div class="field is-grouped">
      <div class="control">
        <button class="button is-link">Create</button>
      </div>
      <div class="control">
        <button class="button is-link is-light">Cancel</button>
      </div>
    </div>
  </form>
</section>

<c:import url = "../_footer.jsp"/>
