<%@ page
  info="Page Footer"
  contentType="text/html; charset=UTF-8"
  pageEncoding="UTF-8"
%>
    </main>

    <footer class="footer">
      <div class="container has-text-right">
        <strong>
          &copy;roh
        </strong>
      </div>
    </footer>
  </body>
</html>
